Decided I am going to put rest of video in this and update as go. Will try use readme to seperate ach topic
# Summary of Go
![Summay!](./Images/GoOverview.png)

- developed by google in 2007

##  Why Go

### Evoulution of infastructure
- Multicore processors, cloud infrastucture, big networked computation clusters

- infrastructure became scalable, dynamic more capacity
- multi threading: multiple things at once. each thread running one task. Run in parralel 

----
- Concurrency is about dealing with lot's of things at once
- Developers need to write code to prevent
- Developers need to write code to prevent conflicts
- Go was desinged to run on multiple cores and built to support concurrency 
- best used for performant applications running on scaled, distibuted systems

# Characteristics of Go 
- **Simple and readable**: syntax of a dynamically typed language like Python
- **Efficiency and safety** of a lower level, statically typed language like C++
- Used on backend side of application eg microservices, web applications, database servers eg docker, kubernetes
- its a compiled language and compiles into single binary.
- simplicity and speed 
# Structure
- have to put in package
- fmt is package for print https://pkg.go.dev/fmt
- need a func main() like c++

# Variable
- used to store values
- give a variable a name and reference it everywhere in app
- Makes app more flexible

![Key ](./Images/KeyWords.PNG)

- MUST use all pacakges and variables, otherwise will error
- const: values not allowed to change

# printf
- example how it would look 
```Go
fmt.Printf("Welcome to %v  booking application\n", conferenceName)
```
# Data Types
- most common are Strings and ints
- Each data type can be used differently and behave differently
![DataTypes](./Images/DataTypes.PNG)
- floating point numbers, higher precision used for statistics, money etcc

### Sytactic Sugar
- term used to describe a feature in a language that lets you do something more easily
- makes language "sweeter" for human use
- But doesn't add any new functionality that it didn't already have


#

# Pointers
- values aved in memory on computer
- Needs to find memory address
- Pointers for scan are like such 
```Go
fmt.Scan(&userName) // use pointer so it can store the value in memory
```

# Book Ticket logic

Pretty basic stuff tbh. Just reassigning variables

# Arrays and slices
- Are commonly used for lists

## Arrays

- Arrays in go have fixed size. They use square brackets
- Only one type can be stored

### Adding new Elements
- Adding and accesing elements can be done by index position i.e 0,1,2,3

# Slices

### Problem with Array
- Need a list more dynamic in size
- Abstraction of array
- More flexible and powerful
- Slices are also index-based and have a size, but is resized when needed


Syntax:
```Go
var bookings []string
bookings = append(bookings, firstName+" "+lastName)
```

# loops
- programming languages provide various contrl strucures to control application flow.
- Loop allows to execute codemultiple times
- In go, only a for loop exists

### for each loop
![Field](./Images/field.PNG)
- An **_** is known as a blank identifier which is ignore a variable dont want to use


# If Else Statement &Boolean
- i know I said only for loops exists, but trust me bro (They do ecist though lol. Actually basically saying for true can do without else if ) 
- if is similar to java just no brackets
- end of a loop: continyes as long as condtion is true eg as long as remaining tickets > 0. Sort of does opposite

# User Input validation
- Users may bot enter data correctly
- need makesure our application can handle bad user input
- need to basically check if user input is valid
- && logical and operator. Both must be true fo whole expression to be true
- || logical OR

```GO 
isValidCity := city == "Dublin" || city == "Cork" //positive check 
isInvalidValidCity := city != "Dublin" || city != "Cork" //negative check 
```

- Sort of just think about it logically to use it same as java really

# Switch Statement
- code to be executed when x cty is selected eg 6 cities checking 6 if else is not reallt viable

```GO
//Example code
	city := "Athlone"

	switch city {
	case "New York":
		//execute code for booking conference
	case "Dublin" :
		//execute code for booking conference
	case "Cork" : 
	    //execute code for booking conference
	case "Berlin": 
		//execute code for booking conference		
	case "Athlone": 
		//execute code for booking conference	
	default:
		fmt.Println("No valid city Selected")
	}
```
Could also add multiple into statement i.e using by country for this
```GO
	city := "Athlone"

	switch city {
	case "New York":
		//execute code for booking conference
	case "Dublin", "Cork", "Athlone": 
		//execute code for booking conference
	case "Berlin": 
		//execute code for booking conference
	default: 		
		fmt.Println("No valid city Selected")
	}
}
```


# Functions in Go
- Encapsulate code into own container (= function). Which logically belong together
- Like variable name, you should give a function a descriptive name
- Call the function by its name, whenever you want to execute this block of code
- Every program has at least one function, which is the main
- functions only executed when called
= can call as much as want 
- function is used to reduce code duplicatiom

### function parameters
- info can be passed into functions as params
- Params are also called arguments

```GO
// Examples of argument passing:
func printFirstNames(bookings []string) {
    
    //slice
}
func greetUser(confName string, confTicket int, remaining uint) {
    
    //string, int and uint
}
```
- Its more descriptive which makes it easier to read for other developers who didn't write code aka KEVIN

### Returning values from a function 
- function can return data as result
- So a function can take input, return output
- In go you have define the input & output parameters including its type explicity
- In go you can return multiple values from function
```GO
//returning 1 value from fucntion 

func getFirstNames(bookings []string) []string {
	firstNames := []string{}
	for _, booking := range bookings {
		var names = strings.Fields(booking)

		firstNames = append(firstNames, names[0])

	}
	return firstNames

```
```GO
//returning multiple values from function
func validateUserInput(firstName string, lastName string,  email string, userTickets uint, remainingTickets uint) (bool, bool, bool){
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets
	return isValidName, isValidEmail, isValidTicketNumber
}
```
### Making code cleaner with pacakge level variables
- we can share variables that are shared between multiple functions
- Instead of passing same variables multiple places
- package level variabes defined outside all functions
- ***Best Practice*** define variable as local as possible
-Create variable where you need it
- Cleaner, more readable code
- Descriptive function names
![Pacakge](./Images/PacakgeLevelVariables.PNG)

### More use cases of functions
- Group logic that belongs together
- Reuse logic and so reducing duplication of code 
eg hosting 10 conferences, and needed validate user input could use functions to reuse

# Packages in GO
- Until know we only worked with on file
```
Can I modularize the application?
Application consisiting of multiple go files?
```
- Go is orgranised into packages which is a collection of Go files
- eg if 10 diff conferences, we could reate seperate ones eg singapore.go, dublin.go
- Same code eg user input validation which we could reuse
- all fles could belong to same package

### Files
- say we have this helper .go, to run we need to type 
~~~
go run main.go helper.go
~~~
A simpler way is :
~~~
go run . //runs current directory 
~~~

### Multiple Packages in your application
- in addition to main, could have ui etc 
![KEv](./Images/MultiplePackages.PNG )

### Exporting a variable
- make it available fr all packages in app eg java public private etc
- FGo you have to capitalize functions to do ti
![val](./Images/Validate.PNG)
- Not only can you export functions, you can exort variables, constannts and types

# Scope Rules In GO
3 levels of scope
~~~
Local    Package    Global
~~~
#### Local
- Declaration within function
~~~
Can be used only within function 
~~~
- Declaration withing block (eg for, if-else)
~~~
Can be used only withing that block 
~~~

#### Package
- Declaration outside of all functions
~~~
Can be used everywhere in the same package
~~~

#### Gloal
- Decleratin outside all functions & uppercase first letter
~~~
Can be used everywhere across all packages
~~~

variable scope = Scope is the region o a program, where a defined variable can be accessed

# Maps
- Maps unique keys to values
- You can retrieve the value by using its key later
- All keys have the same data type 
- All values have the same data type

![str](./Images/str.PNG)

- Have to define inital size, although its a slice too so dynamic will be updated

![](./Images/maps.PNG)

# Struct
- Collect different types od data on users
~~~
birthdate :: Date :: 20/08/2002
neswletter :: bool :: true
attendanceNames :: make([]map[string]string, 0) :: [{name: "Olan, email: "olan@gmail.com}, name: "Kevin", email: "KevinCocklins@hotmail.com]
~~~

- May have other entites like coference habing citues date held on, number of people ([]string, []string, int)
- Struct = "Structure" allows us to hold mixed data types

### Type statement - Custom type
- The type keyword creates a new type, with the name you specify
~~~
"Create a type called "UserData" based on a struct of firstName, lastName.."
~~~

### Defining a struct
- Mixed data tye
- defining a structure (which fields) of the user type
- struct comapred to class in java
- More of a lightweoght class which eg doesnt support inheritance


# Concurency in Go
Program flow 
~~~
- Book ticket
- Generate ticket
- send email
~~~

### time - functionality for time
- The *sleep* function stops or blocks the current "thread" **(goroutine)** execution for the defined duration
![concurrency](./Images/concurrency.PNG)
- this is not optimal 
- hence why we need concurency in our applications 
- Atm, program runs like so
![now](./Images/now.PNG)
- When we know some function will take longer, we want to start & execute it in seperate thread
- just add ***go*** infront of fucntion 
- Say if we got rid of infite for loop, ticket will never get sent as after 1 booking made application exits
- Need to tell main to wait. Need use WaitGroup

### WaitGroup
- Waits for launched goroutine to finish
- Package "sync" provides basic synchronization functionality
- ***ADD:*** Sets the number of goroutines to wait for (***increases*** the counter by the provided number)
- ***WAIT:*** Blocaks until the WaitGroup counter is 0
- ***DONE:*** Decrements the WaitGroup counter by 1 So this is called  by the goroutine to indicate its finised 
```GO
//code before defining package, package variables etc

func main() {


//some code
wg.Add(1)
go sendTicket(userTickets, firstName, lastName, email)
//some more code

//the threaded function
func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	var ticket = fmt.Sprintf("%v tickets for %v %v\n", userTickets, firstName, lastName)
	time.Sleep(10 * time.Second)
	fmt.Printf("##################\n")
	fmt.Printf("Sending ticket:\n%vto email address %v\n", ticket, email)
	fmt.Printf("##################\n")
	wg.Done() //here
}

```
### Comparison to other languages
- Writing concurrent code is way more complicated in other languages 
- More overheads
- Creating threads is more expensive compared to goroutines

### Why? Whats exactly different
- Go is using whats called a ***Green Thread***
- Abstraction of an actual thread. Known as goroutine
![threads](./Images/threads.PNG)
![threadss](./Images/threadss.PNG)
![threadsss](./Images/threadsss.PNG)