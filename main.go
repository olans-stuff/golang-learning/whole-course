package main

import (
	"fmt"
	"sync"
	"time"
)

// Our package level variables
var conferenceName = "Shrek"

const conferenceTickets = 50

var remainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {

	greetUser()

	//Make call to get user input, specifiying inputs
	firstName, lastName, email, userTickets := getUserInput()

	//call validator, specifiing inputs and outputs
	isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, userTickets)
	if isValidName && isValidEmail && isValidTicketNumber {

		ticketBooking(userTickets, firstName, lastName, email)

		wg.Add(1)
		go sendTicket(userTickets, firstName, lastName, email)

		// call function for returning first names
		firstNames := getFirstNames()
		fmt.Println()
		fmt.Println("------------------------------------------------")
		fmt.Printf("These are our bookings by first names: %v\n", firstNames)
		fmt.Println("------------------------------------------------")
		fmt.Println()

		if remainingTickets == 0 {
			// end program
			fmt.Println("Unlucky, conference is booked out")
			//break
		}

	} else {
		if !isValidName {
			fmt.Println()
			fmt.Println("First name / last name you entered is too short")
			fmt.Println()
		}
		if !isValidName {
			fmt.Println("Email address you entered doesn't contain '@' symbol")
			fmt.Println()
		}
		if !isValidTicketNumber {
			fmt.Println("Number of tickets you entered is invalid")
			fmt.Println()
		}

	}
	wg.Wait()
}

/* greetUser()
function for greeting user
*/

func greetUser() {
	// Cleaning up greeting Users
	fmt.Println()
	fmt.Println()
	fmt.Println("------------------------------------------------")
	fmt.Printf("Welcome to %v's booking application\n", conferenceName)
	fmt.Println("------------------------------------------------")
	fmt.Println()
	fmt.Println()

	fmt.Printf("We have total of %v tickets and %v are still remaining\n", conferenceTickets, remainingTickets)
	fmt.Println()
	fmt.Println("Get your tickets here to attend")
	fmt.Println()
}

/* getfirstNames()
function for getting firstnames
*/

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames

}

/* validateUserInput()
function for validating user
*/

/* getUserInput()
function for getting user input
*/

func getUserInput() (string, string, string, uint) {

	var firstName string
	var lastName string
	var email string
	var userTickets uint
	// ask user for name
	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)

	//ask for last name
	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)

	//email
	fmt.Println("Enter you're email address: ")
	fmt.Scan(&email)

	//amt tickets want
	fmt.Println("Enter amount of tickets you're looking for")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

/* ticketBooking()
function for booking ticket
*/

func ticketBooking(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	// Create a map for a user
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Println()
	fmt.Println("------------------------------------------------")
	fmt.Printf("Bookings map: %v\n", bookings)
	fmt.Println("------------------------------------------------")
	fmt.Println()
	fmt.Printf("Thank you %v %v for booking %v tickets.\n", firstName, lastName, userTickets)
	fmt.Println()
	fmt.Printf("You will recieve a confirmation of booking at this email: %v\n", email)
	fmt.Println()
	fmt.Printf("%v tickets remianing for %v\n", remainingTickets, conferenceName)
	fmt.Println()
}

func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	var ticket = fmt.Sprintf("%v tickets for %v %v\n", userTickets, firstName, lastName)
	time.Sleep(10 * time.Second)
	fmt.Printf("##################\n")
	fmt.Printf("Sending ticket:\n%vto email address %v\n", ticket, email)
	fmt.Printf("##################\n")
	wg.Done()
}
